package wtf.scala.lectures

import java.util.concurrent.{Executors, ThreadFactory}

import scala.concurrent.ExecutionContext

package object e13 {
  implicit val dbEc = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor((r: Runnable) => {
    val t = new Thread()
    t.setDaemon(true)
    t
  }))
}
