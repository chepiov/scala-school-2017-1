name := "scala-school-2017-1"

version := "1.0"

scalaVersion := "2.12.1"

// scalaVersion := "2.11.8" // for spark and salat

libraryDependencies ++=  Seq("org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.typesafe.akka" %% "akka-actor" % "2.4.17",
  "com.typesafe.akka" % "akka-stream_2.12" % "2.4.17",
  "com.typesafe.akka" %% "akka-http" % "10.0.5",
  "io.spray" %% "spray-json" % "1.3.3",
  "com.h2database" % "h2" % "1.4.194",
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "org.tpolecat" %% "doobie-core" % "0.4.1",
  "org.scalikejdbc" %% "scalikejdbc" % "2.5.1",
  "ch.qos.logback" % "logback-classic" % "1.2.1",
  "com.typesafe.play" %% "anorm" % "2.5.3",
  "org.mongodb" %% "casbah" % "3.1.1"
//  "com.novus" %% "salat" % "1.9.9" // scala 2.11 required
)
